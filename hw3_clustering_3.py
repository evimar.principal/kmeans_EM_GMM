"""
Created on Sun Mar 19 15:15 2017 

@author: evimar
"""
import numpy as np
import random
import math
#from scipy.stats import multivariate_normal
import sys
#from matplotlib import pyplot as plt

def pdf(x, mu, sigma):
    size = len(x)
    if size == len(mu) and (size, size) == sigma.shape:
        det = np.linalg.det(sigma)
        if det == 0:
            raise NameError("The covariance matrix can't be singular")

        norm_const = 1.0/ ( math.pow((2*math.pi),float(size)/2) * det**0.5 )
        x_mu = np.matrix(x - mu)
        inv = np.linalg.inv( sigma)        
        result = math.pow(math.e, -0.5 * (x_mu * inv * x_mu.T))
        return norm_const * result
    else:
        raise NameError("The dimensions of the input don't match")
        
K=3 #number of cluster
d=2

filedata = sys.argv[1]
X = np.genfromtxt (filedata, delimiter=',')


n, d = X.shape

#Inicializamos los k centroids randomly
U = X[random.sample(range(n), K)]

#U = np.array([[5.72316172633, 7.03506602245], [0.0887880161461, 3.5291769851], [5.5084357544, 10.4242009312]])
#
#plt.plot(mus[0,0], mus[0,1],'xr')
#plt.plot(mus[1,0], mus[1,1],'xg')
#plt.plot(mus[2,0], mus[2,1],'xb')
#plt.plot(X[0,0], X[0,1],'.')

for j in range(1,11):      
    #update C
    C = np.array([])
    for x in X:
        dist =[]
        for i in range(K):
            dist.append(np.linalg.norm(x-U[i]))
        C = np.hstack((C, dist.index(min(dist)) + 1))
        
    #now update the centroids U
    U = X[C==1].mean(axis=0)
    for i in range(2, K+1):
        U = np.vstack((U,X[C==i].mean(axis=0)))
    

    #Create the output file of the iteration j
    filename = "centroids-" + str(j)+".csv"
    np.savetxt(filename, U, delimiter=',' )
 
#EM algorithm    
#Inicialice pis uniformly    
pis =  np.array([1/K for i in range(K)])

mus = X[random.sample(range(n), K)]
        
sigmas = [np.identity(d) for i in range(K)]


#mus = np.array([[-0.0407818979679,	0.350655592545],[1.03391556709,	8.99950591741],[5.92093526532,	8.10258914395]])
#pis = np.array([0.359950569514,0.305602403093,0.334447027393])
#sigmas = []
#sigmas.append(np.array([[7.668415964901511561e-01,	1.554561596459304029e-01],[1.554561596459304029e-01,	2.703460042311495837e+00]]))
#sigmas.append(np.array([[4.487828673493546638e+00,	1.698627087790128387e+00],[1.698627087790128387e+00,	3.187507115509362610e+00]]))
#sigmas.append(np.array([[4.265575343016693388e+00,	1.299683252212352702e+00],[1.299683252212352702e+00,	4.328681085381965055e+00]]))


#Asigna aleatoriamente los clusters uniformemente E-sptep
C2 = np.random.randint(1,K+1,n)
          
for j in range(1,11):
#    N = [multivariate_normal(mean=mus[i], cov=sigmas[i]) for i in range(K)]         
    #Llena la matriz de probabilidades phis
    phis = np.array([pis[i]* pdf(X[0], mus[i], sigmas[i]) for i in range(K)])
#    phis = np.array([pis[i]* N[i].pdf(X[0]) for i in range(K)])
    phis = phis/phis.sum()    
    for x in X[1:]:
        phi =  np.array([pis[i]* pdf(x, mus[i], sigmas[i]) for i in range(K)])
#        phi =  np.array([pis[i]* N[i].pdf(x) for i in range(K)])
        phi = phi / phi.sum()
        phis = np.vstack((phis, phi))
    
#M-step update value pis, mus, and sigmas
    
    pis = phis.mean(axis=0)
    nks = phis.sum(axis=0)
    
    mus = phis.transpose().dot(X)   
    mus = np.array([mus[i]/nks[i] for i in range(K)])

    sigmas=[]

    for k in range(K):
        x_mu = X - mus[k]
        sigma = (phis[:,k] * x_mu.transpose()).dot(x_mu)
        sigma = sigma/nks[k]
        sigmas.append(sigma)
        """
        Sigma-[cluster]-[iteration].csv: This is a comma separated file 
        containing the covariance matrix of one Gaussian of the EM-GMM model. 
        If the data is d-dimensional, there should be d rows with d
         entries in each row. There should be 50 total files. 
         For example, "Sigma-2-3.csv" will contain the covariance matrix 
         of the 2nd Gaussian after the 3rd iteration.        
        """
        filename ="Sigma-"+str(k+1)+"-"+str(j)+".csv"
        np.savetxt(filename, sigma, delimiter=',' )
        print(filename)
        print(sigma)
        
#        sigmam=[]
#        sigma = np.zeros((d,d))    
#        for rec in range(n):
#            x_u = X[rec]-mus[k]
#            sigma += phis[rec, k] * np.outer(x_u, x_u) / nks[k]
#        sigmam.append(sigma)
        
    """
    pi-[iteration].csv: This is a comma separated file containing the 
    cluster probabilities of the EM-GMM model. The 
    kth row should contain the  kth probability,  πk, and there should 
    be 5 rows. There should be 10 total files. For example, "pi-3.csv" 
    will contain the cluster probabilities after the 3rd iteration.
    """
    filename ="pi-"+str(j)+".csv"
    np.savetxt(filename, pis.transpose(), delimiter=',' )
    print(filename)
    print(pis)
    """
    mu-[iteration].csv: This is a comma separated file containing the 
    means of each Gaussian of the EM-GMM model. The 
    kth row should contain the kth mean , and there should be 5 rows. 
    There should be 10 total files. For example, "mu-3.csv" will 
    contain the means of each Gaussian after the 3rd iteration.
    """
    filename ="mu-"+str(j)+".csv"
    np.savetxt(filename, mus, delimiter=',' )
    print(filename)
    print(mus)